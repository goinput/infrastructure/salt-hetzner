######################################################
#               _____ _   _ _____  _    _ _______    #
#              |_   _| \ | |  __ \| |  | |__   __|   #
#     __ _  ___  | | |  \| | |__) | |  | |  | |      #
#    / _` |/ _ \ | | | . ` |  ___/| |  | |  | |      #
#   | (_| | (_) || |_| |\  | |    | |__| |  | |      #
#    \__, |\___/_____|_| \_|_|     \____/   |_|      #
#     __/ |                                          #
#    |___/                                           #
#                                                    #
######################################################

base:
  '*':
    - basictools
    - buildtools
    - letsencrypt
    - cloudflare
    - motd
    - users
    - ufw	
    - cron
    - timezone
    - hostsfile
    - fail2ban
    - openssh
  'salt*':
    - postfix
  'web*':
    - webskeleton
    - postfix
    - apache
    - php.cli
    - php.composer
    - php.fpm
    - redis
    - php.redis
  