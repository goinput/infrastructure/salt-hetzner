######################################################
#               _____ _   _ _____  _    _ _______    #
#              |_   _| \ | |  __ \| |  | |__   __|   #
#     __ _  ___  | | |  \| | |__) | |  | |  | |      #
#    / _` |/ _ \ | | | . ` |  ___/| |  | |  | |      #
#   | (_| | (_) || |_| |\  | |    | |__| |  | |      #
#    \__, |\___/_____|_| \_|_|     \____/   |_|      #
#     __/ |                                          #
#    |___/                                           #
#                                                    #
######################################################

webskel_create_dir:
  file.directory:
    - name: /etc/webskel
    - user: root
    - group: root
    - dir_mode: 755
    - file_mode: 644

webskel_copy_files:
    file.recurse:
        - name: /etc/webskel
        - source: salt://files/etc/webskel
        - clean: true 
        - user: root
        - group: root 
        - dir_mode: 755
        - file_mode: 644
        - include_empty: true
        