######################################################
#               _____ _   _ _____  _    _ _______    #
#              |_   _| \ | |  __ \| |  | |__   __|   #
#     __ _  ___  | | |  \| | |__) | |  | |  | |      #
#    / _` |/ _ \ | | | . ` |  ___/| |  | |  | |      #
#   | (_| | (_) || |_| |\  | |    | |__| |  | |      #
#    \__, |\___/_____|_| \_|_|     \____/   |_|      #
#     __/ |                                          #
#    |___/                                           #
#                                                    #
######################################################

install_basic_packages:
  pkg.installed:
    - pkgs:
      - htop
      - screen
      - zip
      - unzip
      - git
      - whois
      - needrestart
      - python3
      - python3-pip
      - python3-gnupg
      - tree
      - mailutils

rng-tools:
  pkg.removed:
    - name: rng-tools

remove_minion_master_conf:
  file.absent:
    - name: /etc/salt/minion.d/99-master-address.conf

remove_master_file_roots:
  file.absent:
    - name: /etc/salt/master.d/file_roots.conf
