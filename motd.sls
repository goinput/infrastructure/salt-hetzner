######################################################
#               _____ _   _ _____  _    _ _______    #
#              |_   _| \ | |  __ \| |  | |__   __|   #
#     __ _  ___  | | |  \| | |__) | |  | |  | |      #
#    / _` |/ _ \ | | | . ` |  ___/| |  | |  | |      #
#   | (_| | (_) || |_| |\  | |    | |__| |  | |      #
#    \__, |\___/_____|_| \_|_|     \____/   |_|      #
#     __/ |                                          #
#    |___/                                           #
#                                                    #
######################################################

#- motd einheitlich fuer alle Hosts setzen #}
/etc/motd:
    file.managed:
        - source: salt://files/etc/motd
        - template: jinja
        - user: root
        - group: root
        - mode: 0644

#### alle anderen motd Meldungen deaktivieren
/etc/update-motd.d:
  file.directory:
    - clean: True
    - user: root
    - group: root
    - dir_mode: 0755
    - backup: minion

# Copy .nanorc to systems
/etc/skel/.nanorc:
    file.managed:
        - source: salt://files/etc/skel/.nanorc
        - user: root
        - group: root
        - mode: 0644
